package textFilter

import collections.Beautify
import collections.Emoticons
import collections.Twitter

object Filter {

  def filterTwitter (str: String): String = {

    @scala.annotation.tailrec
    def recFilter(str: String, chain: Map[String, (String) => String]): String = {
      chain.headOption match {
        case Some(v) =>
          val filteredString = v._2(str)
          recFilter(filteredString, chain.tail)
        case None => str
      }
    }

    val chain = Map[String,(String) => String](
      "f1"-> Twitter.removeRT,
      "f2"-> Twitter.removeNickName,
      "f3"-> Twitter.removeURL,
      "f4"-> Twitter.removeHashtag,
      "f5"-> Twitter.removeSpec,
      "f6"-> Emoticons.removePunctRepetitions,
      "f7"-> Emoticons.removeHorizontalEmoticons,
      "f8"-> Emoticons.removeUnnededPunct,
      "f9"-> Emoticons.normalizeEmoticons,
      "f10"-> Beautify.removeCharRepetitions,
      "f11"-> Beautify.trimEndUpTo15,
      "f12"-> Beautify.removeNSpaces,
      "f13"-> {(i:String) => i.toLowerCase()}
    )

    recFilter(str, chain)
  }
}
