package textFilter.collections

object Emoticons {

  def removePunctRepetitions(str: String): String = {
    // {Punct} это набор !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
    str.replaceAll("((\\p{Punct})\\2{0})\\2+", "$2")
  }

  def removeHorizontalEmoticons(str: String): String = {
    // {Punct} это набор !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
    //val x1 = str.replaceAll("(\\s|\\A)([хХxXтТToOоО0@$%#=*()\\^\\-]+)([-_.]+)([хХxXтТToOоО0@$%#=*()\\^\\-]+)(\\s|\\Z)", " ")
    str.replaceAll("(\\s|\\A)([[\\\\P{L}хХxXтТTоОoO0]&&[^-_.]]+)([-_.]+)([[\\\\P{L}хХxXтТTоОoO0]&&[^-_.]]+)(\\s|\\Z)", " ")
  }

  def normalizeEmoticons(str: String): String = {

    def reverse(str: String): String = {
      val d = str.replaceAll("\\s([(cC])-?([=:;])", "$2)")
      val e = d.replaceAll("\\s([)D])-?([=:;])", "$2(")
      val b = e.replaceAll("\\s([|\\\\/*0oOоО])-?([=:;])", "$2$1")
      e
    }

    def twoSided(str: String): String = {
      str.replaceAll("(([=:;])([=:;3)|\\\\\\/*0(cC]*))([=:;])", "$1")
    }

    def addSpaces(str: String): String = {
      // детектирует смайлы :) и обрамляет пробелом
      val s = str.replaceAll("([=:;]-?[p3PD)|\\\\/*0oOоО(])", " $1 ")
      s
    }

    def restore(str: String): String = {
      // восстанавливает скобки в смайлы для вариантов "вася (" => "вася :("
      str.replaceAll("(^|[^-=:;])([)(])", "$1 :$2 ")
    }

    val q = reverse(str)
    val w = twoSided(q)
    val e = addSpaces(w)
    val f = restore(e)

    f
  }

  def removeUnnededPunct(str: String): String = {
    // {Punct} это набор !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
    // оставляем только то что используется в эмотиконах ()*/\| :;=
    val q = str.replaceAll("[[\\p{Punct}“”«»]&&[^()*/:;=|\\-]]", " ")
    val d = q.replaceAll("[-]", "")
    // удаляем одиночно стоящие символы из */\|–:;= скобки )( не трогаем
    val w = d.replaceAll("(\\A|[^:;=])([–*Dp\\/\\\\|])([^:;=]|\\Z)", "$1$3")
    val e = w.replaceAll("(\\A|[^–()*/\\\\|])([:;=])([^–()Dp*/\\\\|]|\\Z)", "$1$3")
    e
  }

}
