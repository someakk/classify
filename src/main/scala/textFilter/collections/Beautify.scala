package textFilter.collections

object Beautify {

  def removeCharRepetitions(str: String): String = {
    // чистит 3+ повторяющихся символов
    str.replaceAll("(([a-zA-Zа-яА-ЯёË])\\2{1})\\2+", "$2")
  }

  def trimEndUpTo15(str: String): String = {
    str.replaceAll("\\S[a-zA-Zа-яА-ЯёË]{0,15}…\\Z", " ")
  }

  def removeShortWords(str: String): String = {
    str.replaceAll("((\\s).(\\s))|(^.(\\s))|((\\s).$)", " ")
  }

  def removeNSpaces(str: String): String = {
    val d = str.replaceAll("(\\s)+", " ")
    val s = d.replaceAll("^(\\s)+", "")
    val e = s.replaceAll("(\\s)+$", "")
    e
  }

//    // чистка текста
//    var text = removeCharRepetitions(text)
//    //text = removeShortWords(text)
//    text = trimEndUpTo15(text)
//    text = removeNSpaces(text)
//
//    text

}
