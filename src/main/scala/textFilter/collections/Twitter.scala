package textFilter.collections

object Twitter {

  def removeRT(str: String): String = {
    str.replaceAll("RT @.*:\\s", "")
  }

  def removeNickName(str: String): String = {
    //str.replaceAll("((@[a-zA-Z0-9_]+).+\\b)(\\s)?", "")
    str.replaceAll("@[a-zA-Z0-9_]+(\\s)?", "")
  }

  def removeURL(str: String): String = {
    str.replaceAll("(http(s)?[^\\s]+(\\s)?)", "")
  }

  def removeHashtag(str: String): String = {
    str.replaceAll("#[a-zA-Z0-9_]+(\\s)?", "")
  }

  def removeSpec(str: String): String = {
    str.replaceAll("&([a-z]*;)+", "")
  }

}
