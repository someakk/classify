package api.vk.newsFeed

import api.vk.attachments.VkVideo
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkNewsFeedVideo(`type`: String,
                           sourceId: Int,
                           date: Int,
                           video: VkNewsFeedVideoArray)

object VkNewsFeedVideo {
  implicit val NfVideoReads: Reads[VkNewsFeedVideo] = (
    (JsPath \ "type").read[String] and
      (JsPath \ "source_id").read[Int] and
      (JsPath \ "date").read[Int] and
      (JsPath \ "video").read[VkNewsFeedVideoArray]
    )(VkNewsFeedVideo.apply _)
}

case class VkNewsFeedVideoArray(count: Int,
                                items: Array[VkVideo])

object VkNewsFeedVideoArray {
  implicit val NfVideoReadsArrayReads: Reads[VkNewsFeedVideoArray] = (
    (JsPath \ "count").read[Int] and
      (JsPath \ "items").read[Array[VkVideo]]
    )(VkNewsFeedVideoArray.apply _)
}