package api.vk.newsFeed

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkNewsFeedFriend(`type`: String,
                            sourceId: Int,
                            date: Int,
                            friends: VkNewsFeedFriendArray)

object VkNewsFeedFriend {
  implicit val NfFriendReads: Reads[VkNewsFeedFriend] = (
    (JsPath \ "type").read[String] and
      (JsPath \ "source_id").read[Int] and
      (JsPath \ "date").read[Int] and
      (JsPath \ "friends").read[VkNewsFeedFriendArray]
    )(VkNewsFeedFriend.apply _)
}

case class VkNewsFeedFriendArray(count: Int,
                                 items: Array[VkNewsFeedFriendArrayElem])
object VkNewsFeedFriendArray {
  implicit val NfFriendArrayReads: Reads[VkNewsFeedFriendArray] = (
    (JsPath \ "count").read[Int] and
      (JsPath \ "items").read[Array[VkNewsFeedFriendArrayElem]]
    )(VkNewsFeedFriendArray.apply _)
}

case class VkNewsFeedFriendArrayElem(user_id: String)
object VkNewsFeedFriendArrayElem {
  implicit val NfFriendArrayElemReads: Reads[VkNewsFeedFriendArrayElem] =
    for {
      user <- (JsPath \ "user_id").read[String]
    } yield {
      VkNewsFeedFriendArrayElem(user)
    }
}