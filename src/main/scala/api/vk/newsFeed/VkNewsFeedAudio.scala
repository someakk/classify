package api.vk.newsFeed

import api.vk.attachments.VkAudio
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkNewsFeedAudio(`type`: String,
                           sourceId: Int,
                           date: Int,
                           video: VkNewsFeedVideoArray)

object VkNewsFeedAudio {
  implicit val NfAudioReads: Reads[VkNewsFeedAudio] = (
    (JsPath \ "type").read[String] and
      (JsPath \ "source_id").read[Int] and
      (JsPath \ "date").read[Int] and
      (JsPath \ "audio").read[VkNewsFeedVideoArray]
    )(VkNewsFeedAudio.apply _)
}

case class VkNewsFeedAudioArray(count: Int,
                                items: Array[VkAudio])

object VkWallAudioArray {
  implicit val NfAudioArrayReads: Reads[VkNewsFeedAudioArray] = (
    (JsPath \ "count").read[Int] and
      (JsPath \ "items").read[Array[VkAudio]]
    )(VkNewsFeedAudioArray.apply _)
}