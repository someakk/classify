package api.vk.newsFeed

import api.vk.attachments.VkPhoto
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkNewsFeedPhoto(`type`: String,
                           sourceId: Int,
                           date: Int,
                           photos: VkNewsFeedPhotoArray,
                           postId: Int)
object VkNewsFeedPhoto {
  implicit val NfPhotoReads: Reads[VkNewsFeedPhoto] = (
    (JsPath \ "type").read[String] and
      (JsPath \ "source_id").read[Int] and
      (JsPath \ "date").read[Int] and
      (JsPath \ "photos").read[VkNewsFeedPhotoArray] and
      (JsPath \ "post_id").read[Int]
    )(VkNewsFeedPhoto.apply _)
}

case class VkNewsFeedPhotoArray(count: Int,
                                items: Array[VkPhoto])
object VkNewsFeedPhotoArray {
  implicit val NfPhotoArrayReads: Reads[VkNewsFeedPhotoArray] = (
    (JsPath \ "count").read[Int] and
      (JsPath \ "items").read[Array[VkPhoto]]
    )(VkNewsFeedPhotoArray.apply _)
}