package api.vk.newsFeed

import api.vk.attachments.{VkNote, VkPhoto}
import api.vk.newsFeed.vkNewsFeedPost.VkNewsFeedPost
import play.api.libs.json.{JsError, JsPath, JsSuccess, Reads}

// перечень типов новостей, раздел filters
// https://vk.com/dev/newsfeed.get

trait FeedItemLike [T] {}
object FeedItemLike {
  implicit object FeedItemLikeVkPost extends FeedItemLike[VkNewsFeedPost]
  implicit object FeedItemLikeVkAudio extends FeedItemLike[VkNewsFeedAudio]
  implicit object FeedItemLikeVkFriend extends FeedItemLike[VkNewsFeedFriend]
  implicit object FeedItemLikeVkPhoto extends FeedItemLike[VkNewsFeedPhoto]
  implicit object FeedItemLikeVkVideo extends FeedItemLike[VkNewsFeedVideo]
}

case class VkNewsFeedItem(feedItem:Any)
object VkNewsFeedItem {
  implicit val NfItemReads: Reads[VkNewsFeedItem] = (jsObject) => {

    // get object type through String validation
    val tPath = (JsPath \ "type").read[String]
    val tValidated = jsObject.validate[String](tPath)
    val objectType = tValidated.get

    // object of tValidated type validation
    val `object` = objectType match {
      case "video" => jsObject.validate[VkNewsFeedVideo]((JsPath).read[VkNewsFeedVideo]) match {
          case JsSuccess(obj, _) => obj
          case e: JsError => e
        }
      case "audio" => jsObject.validate[VkNewsFeedAudio]((JsPath).read[VkNewsFeedAudio]) match {
          case JsSuccess(obj, _) => obj
          case e: JsError => e
        }
      case "note" => jsObject.validate[VkNote]((JsPath).read[VkNote]) match {
          case JsSuccess(obj, _) => obj
          case e: JsError => e
        }
      case "post" => jsObject.validate[VkNewsFeedPost]((JsPath).read[VkNewsFeedPost]) match {
          case JsSuccess(obj, _) => obj
          case e: JsError => e
        }
      case "photo" => jsObject.validate[VkPhoto]((JsPath).read[VkPhoto]) match {
          case JsSuccess(obj, _) => obj
          case e: JsError => e
        }
//      case "photo_tag" => jsObject.validate[vkPhotoTag]((JsPath).read[vkPhotoTag]) match {
//          case JsSuccess(obj, _) => obj
//          case e: JsError => e
//        }
      case "wall_photo" => jsObject.validate[VkNewsFeedPhoto]((JsPath).read[VkNewsFeedPhoto]) match {
          case JsSuccess(obj, _) => obj
          case e: JsError => e
        }
      case "friend" => jsObject.validate[VkNewsFeedFriend]((JsPath).read[VkNewsFeedFriend]) match {
          case JsSuccess(obj, _) => obj
          case e: JsError => e
        }
      case undefined @ _ =>
        JsError(JsPath, "Undefined object: " + undefined)
    }
    JsSuccess(VkNewsFeedItem(`object`),JsPath)
  }
}