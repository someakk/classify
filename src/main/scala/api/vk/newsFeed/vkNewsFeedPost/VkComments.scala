package api.vk.newsFeed.vkNewsFeedPost

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkComments(count: Int,
                      canPost: Int,
                      groupsCanPost: Boolean)
object VkComments {
  implicit val vkCommentsReads: Reads[VkComments] = (
    (JsPath \ "count").read[Int] and
      (JsPath \"can_post").read[Int] and
      (JsPath \"groups_can_post").read[Boolean]
    )(VkComments.apply _)
}
