package api.vk.newsFeed.vkNewsFeedPost

import play.api.libs.json.{JsPath, Reads}

case class VkViews(count: Int)
object VkViews {
  implicit val vkViewsReads: Reads[VkViews] =
    for {
      count <- (JsPath \ "count").read[Int]
    } yield {
      VkViews(count)
    }
}