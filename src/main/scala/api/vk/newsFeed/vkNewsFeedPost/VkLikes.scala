package api.vk.newsFeed.vkNewsFeedPost

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkLikes(count: Int,
                   useLikes: Int,
                   canLike: Int,
                   canPublish: Int)
object VkLikes {
  implicit val vkLikesReads: Reads[VkLikes] = (
    (JsPath \ "count").read[Int] and
      (JsPath \"user_likes").read[Int] and
      (JsPath \"can_like").read[Int] and
      (JsPath \"can_publish").read[Int]
    )(VkLikes.apply _)
}