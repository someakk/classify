package api.vk.newsFeed.vkNewsFeedPost

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkReposts(count: Int,
                     userReposted: Int)
object VkReposts {
  implicit val vkRepostsReads: Reads[VkReposts] = (
    (JsPath \ "count").read[Int] and
      (JsPath \ "user_reposted").read[Int]
    )(VkReposts.apply _)
}
