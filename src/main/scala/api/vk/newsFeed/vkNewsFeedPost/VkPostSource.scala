package api.vk.newsFeed.vkNewsFeedPost

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

//https://vk.com/dev/objects/post_source
case class VkPostSource(`type`: String,
                        platform: Option[String],
                        data: Option[String],
                        url: Option[String])
object VkPostSource {
  implicit val vkPostSourceReads: Reads[VkPostSource] = (
    (JsPath \"type").read[String] and
      (JsPath \"platform").readNullable[String] and
      (JsPath \"data").readNullable[String] and
      (JsPath \"url").readNullable[String]
    )(VkPostSource.apply _)
}