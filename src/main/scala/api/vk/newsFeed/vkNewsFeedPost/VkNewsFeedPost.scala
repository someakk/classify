package api.vk.newsFeed.vkNewsFeedPost

import api.vk.attachments.BaseAttach
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}


//todo: полностью не проверяет: post_id, final_post, поля copy_*, поля can_*, geo,
//  частично проверяет: attachments, post_source
case class VkNewsFeedPost(canDoubtCategory: Boolean,
                          canSetCategory: Boolean,
                          `type`: String,
                          sourceId: Int,
                          date: Int,
                          postType: String,
                          text: String,
                          markedAsAds: Int,
                          signerId: Option[Int],
                          attachments: Array[BaseAttach],
                          postSource: VkPostSource,
                          comments: VkComments,
                          likes: VkLikes,
                          reposts: VkReposts,
                          views: VkViews,
                          isFavorite: Boolean,
                          postId: Int)
object VkNewsFeedPost {
  implicit val NfPostReads: Reads[VkNewsFeedPost] = (
    (JsPath \ "can_doubt_category").read[Boolean] and
      (JsPath \ "can_set_category").read[Boolean] and
      (JsPath \ "type").read[String] and
      (JsPath \ "source_id").read[Int] and
      (JsPath \ "date").read[Int] and
      (JsPath \ "post_type").read[String] and
      (JsPath \ "text").read[String] and
      (JsPath \ "marked_as_ads").read[Int] and
      (JsPath \ "signer_id").readNullable[Int] and
      (JsPath \ "attachments").read[Array[BaseAttach]] and
      (JsPath \ "post_source").read[VkPostSource] and
      (JsPath \ "comments").read[VkComments] and
      (JsPath \ "likes").read[VkLikes] and
      (JsPath \ "reposts").read[VkReposts] and
      (JsPath \ "views").read[VkViews] and
      (JsPath \ "is_favorite").read[Boolean] and
      (JsPath \ "post_id").read[Int]
    )(VkNewsFeedPost.apply _)
}