package api.vk

import api.vk.newsFeed.VkNewsFeedItem
import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

// ответ с сервера
//todo: не описан полностью объекты: items, profiles, groups
case class VkResponse(items: Array[VkNewsFeedItem],
                      profiles: Array[VkUser],
                      groups: Array[VkCommunity],
                      nextFrom: Option[String])
object VkResponse {
  implicit val vkUserReads: Reads[VkResponse] = (
    (JsPath \ "items").read[Array[VkNewsFeedItem]] and
      (JsPath \ "profiles").read[Array[VkUser]] and
      (JsPath \ "groups").read[Array[VkCommunity]] and
      (JsPath \ "next_from").readNullable[String]
    )(VkResponse.apply _)
}