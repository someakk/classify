package api.vk

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkCommunity(id: Int,
                       name: String,
                       screenName: String,
                       isClosed: Int,
                       `type`: String,
                       isAdmin: Int,
                       isMember: Int,
                       isAdvertiser: Int,
                       photo50: String,
                       photo100: String,
                       photo200: String)
object VkCommunity {
  implicit val vkCommunityReads: Reads[VkCommunity] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "name").read[String] and
      (JsPath \ "screen_name").read[String] and
      (JsPath \ "is_closed").read[Int] and
      (JsPath \ "type").read[String] and
      (JsPath \ "is_admin").read[Int] and
      (JsPath \ "is_member").read[Int] and
      (JsPath \ "is_advertiser").read[Int] and
      (JsPath \ "photo_50").read[String] and
      (JsPath \ "photo_100").read[String] and
      (JsPath \ "photo_200").read[String]
    )(VkCommunity.apply _)
}