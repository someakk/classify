package api.vk

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkUser(id: Int,
                  firstName: String,
                  lastName: String,
                  isClosed: Boolean,
                  canAccessClosed: Boolean,
                  photo100: String)
object VkUser {
  implicit val vkUserReads: Reads[VkUser] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "first_name").read[String] and
      (JsPath \ "last_name").read[String] and
      (JsPath \ "is_closed").read[Boolean] and
      (JsPath \ "can_access_closed").read[Boolean] and
      (JsPath \ "photo_100").read[String]
    )(VkUser.apply _)
}