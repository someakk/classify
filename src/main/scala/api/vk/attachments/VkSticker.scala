package api.vk.attachments

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkSticker(productId: Int,
                     stickerId: Int,
                     images: Array[VkStickerImage],
                     imagesWithBackground: Array[VkStickerImageBlack])
object VkSticker {
  implicit val vkStickerReads: Reads[VkSticker] = (
    (JsPath \"product_id").read[Int] and
      (JsPath \"sticker_id").read[Int] and
      (JsPath \"images").read[Array[VkStickerImage]] and
      (JsPath \"images_with_background").read[Array[VkStickerImageBlack]]
    )(VkSticker.apply _)
}

case class VkStickerImage(url: String,
                          width: Int,
                          height: Int)
object VkStickerImage {
  implicit val vkStickerImageReads: Reads[VkStickerImage] = (
    (JsPath \"url").read[String] and
      (JsPath \"width").read[Int] and
      (JsPath \"height").read[Int]
    )(VkStickerImage.apply _)
}

case class VkStickerImageBlack(url: String,
                               width: Int,
                               height: Int)
object VkStickerImageBlack {
  implicit val vkStickerImageBlackReads: Reads[VkStickerImageBlack] = (
    (JsPath \"url").read[String] and
      (JsPath \"width").read[Int] and
      (JsPath \"height").read[Int]
    )(VkStickerImageBlack.apply _)
}