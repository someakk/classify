package api.vk.attachments

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkMarket(id: Int,
                    ownerId: Int,
                    title: String,
                    description: String,
                    price: VkPrice,
                    category: VkCategory,
                    thumbPhoto: String,
                    date: Int,
                    availability: Int,
                    isFavorite: Boolean,
                    extended: Option[Int],
                    photos: Option[Array[VkPhoto]],
                    canComment: Option[Int],
                    canRepost: Option[Int],
                    likes: Option[Int],
                    url: Option[Int],
                    buttonTitle: Option[Int])
object VkMarket {
  implicit val vkMarketReads: Reads[VkMarket] = (
    (JsPath \"id").read[Int] and
      (JsPath \"owner_id").read[Int] and
      (JsPath \"title").read[String] and
      (JsPath \"description").read[String] and
      (JsPath \"price").read[VkPrice] and
      (JsPath \"category").read[VkCategory] and
      (JsPath \"thumb_photo").read[String] and
      (JsPath \"date").read[Int] and
      (JsPath \"availability").read[Int] and
      (JsPath \"is_favorite").read[Boolean] and
      (JsPath \"extended").readNullable[Int] and
      (JsPath \"photos").readNullable[Array[VkPhoto]] and
      (JsPath \"can_comment").readNullable[Int] and
      (JsPath \"can_repost").readNullable[Int] and
      (JsPath \"likes").readNullable[Int] and
      (JsPath \"url").readNullable[Int] and
      (JsPath \"button_title").readNullable[Int]
    )(VkMarket.apply _)
}

case class VkCategory(id: Int,
                      name: String,
                      section: VkSection)
object VkCategory {
  implicit val vkCategoryReads: Reads[VkCategory] = (
    (JsPath \"id").read[Int] and
      (JsPath \"name").read[String] and
      (JsPath \"section").read[VkSection]
    )(VkCategory.apply _)
}

case class VkSection(id: Int, name: String)
object VkSection {
  implicit val vkSectionReads: Reads[VkSection] = (
    (JsPath \"id").read[Int] and
      (JsPath \"name").read[String]
    )(VkSection.apply _)
}