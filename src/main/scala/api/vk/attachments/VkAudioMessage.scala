package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkAudioMessagePreview(duration: Int,
                                 linkOgg: String,
                                 linkMp3: String)
object VkAudioMessagePreview {
  implicit val vkAudioMessagePreviewReads: Reads[VkAudioMessagePreview] = (
    (JsPath \ "duration").read[Int] and
      (JsPath \ "link_ogg").read[String] and
      (JsPath \ "link_mp3").read[String]
    )(VkAudioMessagePreview.apply _)
}