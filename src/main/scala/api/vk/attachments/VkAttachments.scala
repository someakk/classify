package api.vk.attachments

import play.api.libs.json.{JsError, JsPath, JsSuccess, Reads}

// перечень типов аттачментов
// https://vk.com/dev/objects/attachments_w

case class VkAttachments(`type`: String,
                         attach: Array[BaseAttach])
object VkAttachments {
  implicit val vkAttachmentReads: Reads[VkAttachments]  =
    for {
      ttype <- (JsPath \ "type").read[String]
      attach <- (JsPath \ ttype).read[Array[BaseAttach]]
    } yield {
      VkAttachments(ttype, attach)
    }
}

case class BaseAttach (ttype: String,
                       attach: Any)
object BaseAttach {
  implicit val BaseReads: Reads[BaseAttach] = (jsObject) => {
    // type validation
    val tPath = (JsPath \ "type").read[String]
    val tValidated = jsObject.validate[String](tPath)

    // object of type tValidated validation
    val objectType = tValidated.get
    val `object` = objectType match {
      case album @ "album" =>
        val p = (JsPath \ album).read[VkAlbum]
        jsObject.validate[VkAlbum](p) match {
          case JsSuccess(album, _) => album
          case e: JsError => e
        }
      case app @ "app" =>
        val p = (JsPath \ app).read[VkApp]
        jsObject.validate[VkApp](p) match {
          case JsSuccess(app, _) => app
          case e: JsError => e
        }
      case audio @ "audio" =>
        val p = (JsPath \ audio).read[VkAudio]
        jsObject.validate[VkAudio](p) match {
          case JsSuccess(audio, _) => audio
          case e: JsError => e
        }
      case doc @ "doc" =>
        val p = (JsPath \ doc).read[VkDoc]
        jsObject.validate[VkDoc](p) match {
          case JsSuccess(doc, _) => doc
          case e: JsError => e
        }
      case event @ "event" =>
        val p = (JsPath \ event).read[VkEvent]
        jsObject.validate[VkEvent](p) match {
          case JsSuccess(event, _) => event
          case e: JsError => e
        }
      case graffiti @ "graffiti" =>
        val p = (JsPath \ graffiti).read[VkGraffiti]
        jsObject.validate[VkGraffiti](p) match {
          case JsSuccess(graffiti, _) => graffiti
          case e: JsError => e
        }
      case link @ "link" =>
        val p = (JsPath \ link).read[VkLink]
        jsObject.validate[VkLink](p) match {
          case JsSuccess(link, _) => link
          case e: JsError => e
        }
      case market @ "market" =>
        val p = (JsPath \ market).read[VkMarket]
        jsObject.validate[VkMarket](p) match {
          case JsSuccess(market, _) => market
          case e: JsError => e
        }
      case marketAlbum @ "market_album" =>
        val p = (JsPath \ marketAlbum).read[VkMarketAlbum]
        jsObject.validate[VkMarketAlbum](p) match {
          case JsSuccess(marketAlbum, _) => marketAlbum
          case e: JsError => e
        }
      case note @ "note" =>
        val p = (JsPath \ note).read[VkNote]
        jsObject.validate[VkNote](p) match {
          case JsSuccess(note, _) => note
          case e: JsError => e
        }
      case page @ "page" =>
        val p = (JsPath \ page).read[VkPage]
        jsObject.validate[VkPage](p) match {
          case JsSuccess(page, _) => page
          case e: JsError => e
        }
      case photo @ "photo" =>
        val p = (JsPath \ photo).read[VkPhoto]
        jsObject.validate[VkPhoto](p) match {
          case JsSuccess(photo, _) => photo
          case e: JsError => e
        }
      case photosList @ "photos_list" =>
        val p = (JsPath \ photosList).read[VkPhotosList]
        jsObject.validate[VkPhotosList](p) match {
          case JsSuccess(photosList, _) => photosList
          case e: JsError => e
        }
      case pool @ "pool" =>
        val p = (JsPath \ pool).read[VkPool]
        jsObject.validate[VkPool](p) match {
          case JsSuccess(pool, _) => pool
          case e: JsError => e
        }
      case postedPhoto @ "posted_photo" =>
        val p = (JsPath \ postedPhoto).read[VkPostedPhoto]
        jsObject.validate[VkPostedPhoto](p) match {
          case JsSuccess(postedPhoto, _) => postedPhoto
          case e: JsError => e
        }
      case prettyCards @ "pretty_cards" =>
        val p = (JsPath \ prettyCards).read[VkPrettyCards]
        jsObject.validate[VkPrettyCards](p) match {
          case JsSuccess(prettyCards, _) => prettyCards
          case e: JsError => e
        }
      case photo @ "sticker" =>
        val p = (JsPath \ photo).read[VkSticker]
        jsObject.validate[VkSticker](p) match {
          case JsSuccess(sticker, _) => sticker
          case e: JsError => e
        }
      case video @ "video" =>
        val p = (JsPath \ video).read[VkVideo]
        jsObject.validate[VkVideo](p) match {
          case JsSuccess(video, _) => video
          case e: JsError => e
        }
      case undefined @ _ =>
        JsError(JsPath, "Undefined object: " + undefined)
    }
    JsSuccess(BaseAttach(objectType,`object`),JsPath)
  }
}
