package api.vk.attachments

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkPostedPhoto(id: Int,
                         ownerId: Int,
                         photo130: String,
                         photo604: String)
object VkPostedPhoto {
  implicit val vkPostedPhotoReads: Reads[VkPostedPhoto] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "owner_id").read[Int] and
      (JsPath \ "photo_130").read[String] and
      (JsPath \"photo_604").read[String]
    )(VkPostedPhoto.apply _)
}