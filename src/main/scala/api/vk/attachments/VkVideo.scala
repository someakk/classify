package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkVideo(id: Int,
                   ownerId: Int,
                   duration: Int,
                   date: Int,
                   comments: Int,
                   views: Int,
                   width: Option[Int],
                   height: Option[Int],
                   canAdd: Int,
                   title: String,
                   description: String,
                   photo130: String,
                   photo320: String,
                   firstFrame130: Option[String],
                   firstFrame320: Option[String],
                   //---
                   addingDate: Option[Int],
                   canEdit: Option[Int],
                   isPrivate: Option[Int],
                   accessKey: Option[Int],
                   processing: Option[Int],
                   live: Option[Int],
                   upcoming: Option[Int],
                   //---
                   is_favorite: Option[Boolean],
                   player: Option[String],
                   platform: Option[String],
                   first_frame_640: Option[String],
                   first_frame_800: Option[String],
                   first_frame_1280: Option[String],
                   photo_640: Option[String],
                   photo_800: Option[String],
                   photo_1280: Option[String]
                  )
object VkVideo {

  implicit val hugeVkVidoeReads: Reads[VkVideo] = {

    val fields1to22: Reads[
      (Int, Int, Int, Int, Int, Int, Option[Int], Option[Int], Int,
        String, String, String, String, Option[String], Option[String],
        Option[Int], Option[Int], Option[Int], Option[Int], Option[Int], Option[Int], Option[Int])
    ] = (
      (JsPath \ "id").read[Int] and
        (JsPath \ "owner_id").read[Int] and
        (JsPath \ "duration").read[Int] and
        (JsPath \ "date").read[Int] and
        (JsPath \ "comments").read[Int] and
        (JsPath \ "views").read[Int] and
        (JsPath \ "width").readNullable[Int] and
        (JsPath \ "height").readNullable[Int] and
        (JsPath \ "can_add").read[Int] and
        (JsPath \ "title").read[String] and
        (JsPath \ "description").read[String] and
        (JsPath \ "photo_130").read[String] and
        (JsPath \ "photo_320").read[String] and
        (JsPath \ "first_frame_130").readNullable[String] and
        (JsPath \ "first_frame_320").readNullable[String] and
        (JsPath \ "adding_date").readNullable[Int] and
        (JsPath \ "can_edit").readNullable[Int] and
        (JsPath \ "is_private").readNullable[Int] and
        (JsPath \ "access_key").readNullable[Int] and
        (JsPath \ "processing").readNullable[Int] and
        (JsPath \ "live").readNullable[Int] and
        (JsPath \ "upcoming").readNullable[Int]
      ).tupled

    val fields23to31: Reads[
      (Option[Boolean], Option[String], Option[String],
        Option[String], Option[String], Option[String],
        Option[String], Option[String], Option[String])
    ] = (
      (JsPath \ "is_favorite").readNullable[Boolean] and
        (JsPath \ "player").readNullable[String] and
        (JsPath \ "platform").readNullable[String] and
        (JsPath \ "first_frame_640").readNullable[String] and
        (JsPath \ "first_frame_800").readNullable[String] and
        (JsPath \ "first_frame_1280").readNullable[String] and
        (JsPath \ "photo_640").readNullable[String] and
        (JsPath \ "photo_800").readNullable[String] and
        (JsPath \ "photo_1280").readNullable[String]
      ).tupled

    (fields1to22 and fields23to31) (
      (part1, part2) => {
        VkVideo(
          part1._1,
          part1._2,
          part1._3,
          part1._4,
          part1._5,
          part1._6,
          part1._7,
          part1._8,
          part1._9,
          part1._10,
          part1._11,
          part1._12,
          part1._13,
          part1._14,
          part1._15,
          part1._16,
          part1._17,
          part1._18,
          part1._19,
          part1._20,
          part1._21,
          part1._22,
          part2._1,
          part2._2,
          part2._3,
          part2._4,
          part2._5,
          part2._6,
          part2._7,
          part2._8,
          part2._9)
      }
    )
  }

  case class VkVideoPreview(src: String,
                            width: Int,
                            height: Int,
                            fileSize: Int)
  object VkVideoPreview {
    implicit val vkVideoPreviewReads: Reads[VkVideoPreview] = (
      (JsPath \ "src").read[String] and
        (JsPath \ "width").read[Int] and
        (JsPath \ "height").read[Int] and
        (JsPath \ "file_size").read[Int]
      ) (VkVideoPreview.apply _)
  }

}