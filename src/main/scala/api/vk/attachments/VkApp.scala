package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkApp(id: Int,
                 name: String,
                 photo130: String,
                 photo604: String)
object VkApp {
  implicit val vkAppReads: Reads[VkApp] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "name").read[String] and
      (JsPath \ "photo_130").read[String] and
      (JsPath \ "photo_604").read[String]
    )(VkApp.apply _)
}