package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkAlbum(id: String,
                   thumb: VkPhoto,
                   ownerId: Int,
                   title: String,
                   description: String,
                   created: Int,
                   updated: Int,
                   size: Int)
object VkAlbum {
  implicit val vkAlbumReads: Reads[VkAlbum] = (
    (JsPath \"id").read[String] and
      (JsPath \"thumb").read[VkPhoto] and
      (JsPath \"owner_id").read[Int] and
      (JsPath \"title").read[String] and
      (JsPath \"description").read[String] and
      (JsPath \"created").read[Int] and
      (JsPath \"updated").read[Int] and
      (JsPath \"size").read[Int]
    )(VkAlbum.apply _)
}