package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkPage(id: Int,
                  groupId: Int,
                  creatorId: Int,
                  title: String,
                  currentUserCanEdit: Int,
                  currentUserCanEditAccess: Int,
                  whoCanView: Int,
                  whoCanEdit: Int,
                  edited: Int,
                  created: Int,
                  editorId: Int,
                  views: Int,
                  parent: String,
                  parent2: String,
                  source: String,
                  html: String,
                  viewUrl: String)
object VkPage {
  implicit val vkPageReads: Reads[VkPage] = (
    (JsPath \"id").read[Int] and
      (JsPath \"group_id").read[Int] and
      (JsPath \"creator_id").read[Int] and
      (JsPath \"title").read[String] and
      (JsPath \"current_user_can_edit").read[Int] and
      (JsPath \"current_user_can_edit_access").read[Int] and
      (JsPath \"who_can_view").read[Int] and
      (JsPath \"who_can_edit").read[Int] and
      (JsPath \"edited").read[Int] and
      (JsPath \"created").read[Int] and
      (JsPath \"editor_id").read[Int] and
      (JsPath \"views").read[Int] and
      (JsPath \"parent").read[String] and
      (JsPath \"parent2").read[String] and
      (JsPath \"source").read[String] and
      (JsPath \"html").read[String] and
      (JsPath \"view_url").read[String]
    )(VkPage.apply _)
}
