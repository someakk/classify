package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}

case class VkPhotosList(photos: Array[String])
object VkPhotosList {
  implicit val vkPhotoPreviewReads: Reads[VkPhotosList] =
    for {
      photos <- (JsPath).read[Array[String]]
    } yield {
      VkPhotosList(photos)
    }
}