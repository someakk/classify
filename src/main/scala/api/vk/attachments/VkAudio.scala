package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkAudio(id: Int,
                   ownerId: Int,
                   artist: String,
                   title: String,
                   duration: Int,
                   url: String,
                   lyricsId: Option[Int],
                   albumId: Option[Int],
                   genreId: Int,
                   date: Int,
                   isHq: Option[Boolean],
                   noSearch: Option[Boolean])
object VkAudio {
  implicit val vkAudioReads: Reads[VkAudio] = (
    (JsPath \"id").read[Int] and
      (JsPath \"owner_id").read[Int] and
      (JsPath \"artist").read[String] and
      (JsPath \"title").read[String] and
      (JsPath \"duration").read[Int] and
      (JsPath \"url").read[String] and
      (JsPath \"lyrics_id").readNullable[Int] and
      (JsPath \"album_id").readNullable[Int] and
      (JsPath \"genre_id").read[Int] and
      (JsPath \"date").read[Int] and
      (JsPath \"is_hq").readNullable[Boolean] and
      (JsPath \"no_search").readNullable[Boolean]
    )(VkAudio.apply _)
}