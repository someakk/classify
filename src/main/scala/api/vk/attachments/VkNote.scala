package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkNote(id: Int,
                  ownerId: Int,
                  title: String,
                  text: String,
                  date: Int,
                  comments: Int,
                  readComments: Int,
                  viewUrl: String)
object VkNote {
  implicit val vkNoteReads: Reads[VkNote] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "owner_id").read[Int] and
      (JsPath \ "title").read[String] and
      (JsPath \ "text").read[String] and
      (JsPath \ "date").read[Int] and
      (JsPath \ "comments").read[Int] and
      (JsPath \ "read_comments").read[Int] and
      (JsPath \ "url").read[String]
    )(VkNote.apply _)
}