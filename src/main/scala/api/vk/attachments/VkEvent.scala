package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkEvent(id: Int,
                   time: Int,
                   memberStatus: Int,
                   isFavorite: Boolean,
                   address: String,
                   text: String,
                   buttonText: String,
                   friends: Array[Int])
object VkEvent {
  implicit val vkEventReads: Reads[VkEvent] = (
    (JsPath \"id").read[Int] and
      (JsPath \"time").read[Int] and
      (JsPath \"member_status").read[Int] and
      (JsPath \"is_favorite").read[Boolean] and
      (JsPath \"address").read[String] and
      (JsPath \"text").read[String] and
      (JsPath \"button_text").read[String] and
      (JsPath \"friends").read[Array[Int]]
    )(VkEvent.apply _)
}