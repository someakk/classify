package api.vk.attachments

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkPhoto(id: Int,
                   albumId: Int,
                   ownerId: Int,
                   userId: Int,
                   text: String,
                   date: Int,
                   sizes: Array[VkPhotoSize],
                   width: Option[Int],
                   height: Option[Int])
object VkPhoto {
  implicit val vkPhotoReads: Reads[VkPhoto] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "album_id").read[Int] and
      (JsPath \ "owner_id").read[Int] and
      (JsPath \ "user_id").read[Int] and
      (JsPath \"text").read[String] and
      (JsPath \"date").read[Int] and
      (JsPath \"sizes").read[Array[VkPhotoSize]] and
      (JsPath \"width").readNullable[Int] and
      (JsPath \"height").readNullable[Int]
    )(VkPhoto.apply _)
}

case class VkPhotoSize(url: String,
                       width: Int,
                       height: Int,
                       `type`: String)
object VkPhotoSize {
  implicit val vkPhotoSizeReads: Reads[VkPhotoSize] = (
    (JsPath \ "url").read[String] and
      (JsPath \ "width").read[Int] and
      (JsPath \ "height").read[Int] and
      (JsPath \ "type").read[String]
    )(VkPhotoSize.apply _)
}

case class VkPhotoPreview(sizes: VkPhotoSize)
object VkPhotoPreview {
  implicit val vkPhotoPreviewReads: Reads[VkPhotoPreview] =
    for {
      sizes <- (JsPath \ "sizes").read[VkPhotoSize]
    } yield {
      VkPhotoPreview(sizes)
    }
}