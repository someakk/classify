package api.vk.attachments

import api.vk.attachments.VkVideo.VkVideoPreview
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkDoc(id: Int,
                 ownerId: Int,
                 title: String,
                 size: Int,
                 ext: String,
                 url: String,
                 date: Int,
                 `type`: Int,
                 preview: VkDocPreview)
object VkDoc {
  implicit val vkDocReads: Reads[VkDoc] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "owner_id").read[Int] and
      (JsPath \ "title").read[String] and
      (JsPath \ "size").read[Int] and
      (JsPath \ "ext").read[String] and
      (JsPath \ "url").read[String] and
      (JsPath \ "date").read[Int] and
      (JsPath \ "type").read[Int] and
      (JsPath \ "preview").read[VkDocPreview]
    )(VkDoc.apply _)
}

case class VkDocPreview(photo: Option[VkPhotoPreview],
                        video: Option[VkVideoPreview],
                        graffiti: Option[VkGraffitiPreview],
                        audioMessage: Option[VkAudioMessagePreview])
object VkDocPreview {
  implicit val vkDocPreviewReads: Reads[VkDocPreview] = (
    (JsPath \ "photo").readNullable[VkPhotoPreview] and
      (JsPath \ "video").readNullable[VkVideoPreview] and
      (JsPath \ "graffiti").readNullable[VkGraffitiPreview] and
      (JsPath \ "audio_message").readNullable[VkAudioMessagePreview]
    )(VkDocPreview.apply _)
}