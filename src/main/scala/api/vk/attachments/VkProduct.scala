package api.vk.attachments

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkProduct(price: VkPrice)
object VkProduct {
  implicit val vkProductReads: Reads[VkProduct] =
    for {
      price <- (JsPath \ "price").read[VkPrice]
    } yield {
      VkProduct(price)
    }
}

case class VkPrice(amount: Int,
                   currency: VkCurrency,
                   text: String)
object VkPrice {
  implicit val vkPriceReads: Reads[VkPrice] = (
    (JsPath \ "amount").read[Int] and
      (JsPath \ "currency").read[VkCurrency] and
      (JsPath \ "text").read[String]
    )(VkPrice.apply _)
}

case class VkCurrency(id: Int, name: String)
object VkCurrency {
  implicit val vkCurrencyReads: Reads[VkCurrency] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "name").read[String]
    )(VkCurrency.apply _)
}