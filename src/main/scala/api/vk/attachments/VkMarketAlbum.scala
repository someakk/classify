package api.vk.attachments

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkMarketAlbum(id: Int,
                         ownerId: Int,
                         title: String,
                         photo: VkPhoto,
                         count: Int,
                         updatedTime: Int)

object VkMarketAlbum {
  implicit val vkMarketAlbumReads: Reads[VkMarketAlbum] = (
    (JsPath \"id").read[Int] and
      (JsPath \"owner_id").read[Int] and
      (JsPath \"title").read[String] and
      (JsPath \"photo").read[VkPhoto] and
      (JsPath \"count").read[Int] and
      (JsPath \"updated_time").read[Int]
    )(VkMarketAlbum.apply _)
}