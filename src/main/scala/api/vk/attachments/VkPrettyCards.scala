package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkPrettyCards(cardId: String,
                         linkUrl: String,
                         title: String,
                         images: Array[VkStickerImage],
                         button: VkButton,
                         price: String,
                         priceOld: String)
object VkPrettyCards {
  implicit val vkCardsReads: Reads[VkPrettyCards] = (
    (JsPath \"card_id").read[String] and
      (JsPath \"link_url").read[String] and
      (JsPath \"title").read[String] and
      (JsPath \"images").read[Array[VkStickerImage]] and
      (JsPath \"button").read[VkButton] and
      (JsPath \"price").read[String] and
      (JsPath \"price_old").read[String]
    )(VkPrettyCards.apply _)
}