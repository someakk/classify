package api.vk.attachments

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkPool(id: Int,
                  ownerId: Int,
                  authorId: Int,
                  created: Int,
                  votes: Int,
                  endDate: Int,
                  answerIds: Array[Int],
                  friends: Array[Int],
                  question: String,
                  anonymous: Boolean,
                  multiple: Boolean,
                  closed: Boolean,
                  isBoard: Boolean,
                  canEdit: Boolean,
                  canVote: Boolean,
                  canReport: Boolean,
                  canShare: Boolean,
                  answers: VkPoolAnswer,
                  photo: VkPhoto,
                  background: VkPoolBackground)
object VkPool {
  implicit val vkPoolReads: Reads[VkPool] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "owner_id").read[Int] and
      (JsPath \ "author_id").read[Int] and
      (JsPath \ "created").read[Int] and
      (JsPath \ "votes").read[Int] and
      (JsPath \ "end_date").read[Int] and
      (JsPath \ "answer_ids").read[Array[Int]] and
      (JsPath \ "friends").read[Array[Int]] and
      (JsPath \ "question").read[String] and
      (JsPath \ "anonymous").read[Boolean] and
      (JsPath \ "multiple").read[Boolean] and
      (JsPath \ "closed").read[Boolean] and
      (JsPath \ "is_board").read[Boolean] and
      (JsPath \ "can_edit").read[Boolean] and
      (JsPath \ "can_vote").read[Boolean] and
      (JsPath \ "can_report").read[Boolean] and
      (JsPath \ "can_share").read[Boolean] and
      (JsPath \ "answers").read[VkPoolAnswer] and
      (JsPath \ "photo").read[VkPhoto] and
      (JsPath \ "background").read[VkPoolBackground]
    )(VkPool.apply _)
}

case class VkPoolAnswer(id: Int,
                        text: String,
                        votes: Int,
                        rate: Int)
object VkPoolAnswer {
  implicit val vkPoolAnswerReads: Reads[VkPoolAnswer] = (
    (JsPath \"id").read[Int] and
      (JsPath \"text").read[String] and
      (JsPath \"votes").read[Int] and
      (JsPath \"rate").read[Int]
    )(VkPoolAnswer.apply _)
}

case class VkPoolBackground(id: Int,
                            `type`: String,
                            angle: Int,
                            color: String,
                            width: Int,
                            height: Int,
                            images: VkPhotoSize,
                            points: VkPoolBackgroundPoint)
object VkPoolBackground {
  implicit val vkPoolBackgroundReads: Reads[VkPoolBackground] = (
    (JsPath \"id").read[Int] and
      (JsPath \"type").read[String] and
      (JsPath \"angle").read[Int] and
      (JsPath \"color").read[String] and
      (JsPath \"width").read[Int] and
      (JsPath \"height").read[Int] and
      (JsPath \"images").read[VkPhotoSize] and
      (JsPath \"points").read[VkPoolBackgroundPoint]
    )(VkPoolBackground.apply _)
}

case class VkPoolBackgroundPoint(position: Int,
                                 color: String)
object VkPoolBackgroundPoint {
  implicit val vkPoolBackgroundPointReads: Reads[VkPoolBackgroundPoint] = (
    (JsPath \"position").read[Int] and
      (JsPath \"color").read[String]
    )(VkPoolBackgroundPoint.apply _)
}
