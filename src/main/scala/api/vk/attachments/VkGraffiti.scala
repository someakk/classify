package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkGraffiti(id: Int,
                      ownerId: Int,
                      photo130: String,
                      photo604: String)
object VkGraffiti {
  implicit val vkGraffitiReads: Reads[VkGraffiti] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "owner_id").read[Int] and
      (JsPath \ "photo_130").read[String] and
      (JsPath \ "photo_604").read[String]
    ) (VkGraffiti.apply _)
}

case class VkGraffitiPreview(src: String,
                             width: Int,
                             height: Int)
object VkGraffitiPreview {
  implicit val vkGraffitiPreviewReads: Reads[VkGraffitiPreview] = (
    (JsPath \ "src").read[String] and
      (JsPath \ "width").read[Int] and
      (JsPath \ "height").read[Int]
    )(VkGraffitiPreview.apply _)
}