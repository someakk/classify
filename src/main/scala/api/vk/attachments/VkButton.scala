package api.vk.attachments

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class VkButton(title: String, action: VkAction)
object VkButton {
  implicit val vkButtonReads: Reads[VkButton] = (
    (JsPath \ "title").read[String] and
      (JsPath \ "action").read[VkAction]
    )(VkButton.apply _)
}

case class VkAction(`type`: String, url: String)
object VkAction {
  implicit val vkActionReads: Reads[VkAction] = (
    (JsPath \ "type").read[String] and
      (JsPath \ "url").read[String]
    )(VkAction.apply _)
}
