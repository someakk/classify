package api.vk.attachments

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class VkLink(url: Int,
                  title: Int,
                  description: String,
                  caption: Option[String],
                  photo: Option[VkPhoto],
                  product: Option[VkProduct],
                  button: Option[VkButton],
                  previewPage: Option[String],
                  previewUrl: Option[String],
                  target: Option[String])
object VkLink {
  implicit val vkLinkReads: Reads[VkLink] = (
    (JsPath \ "url").read[Int] and
      (JsPath \ "title").read[Int] and
      (JsPath \ "description").read[String] and
      (JsPath \ "caption").readNullable[String] and
      (JsPath \ "photo").readNullable[VkPhoto] and
      (JsPath \ "product").readNullable[VkProduct] and
      (JsPath \ "button").readNullable[VkButton] and
      (JsPath \ "preview_page").readNullable[String] and
      (JsPath \ "preview_url").readNullable[String] and
      (JsPath \ "target").readNullable[String]
    )(VkLink.apply _)
}