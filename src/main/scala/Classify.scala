import naiveBayesClassifier.NaiveBayesLearningAlgorithm
import naiveBayesClassifier.utils._
import jsonReader.FileReader._
import jsonReader.ApiVkReader._
import textFilter.Filter._
import api.vk._
import api.vk.newsFeed.VkNewsFeedAudio
import api.vk.newsFeed.vkNewsFeedPost.VkNewsFeedPost

object Classify extends  App{

  // создать модель
  val pathToBase = "data/classificatorTexts/base.csv"
  val patternToParse = "(^\".*\"),(\".*\")"
  val c = new NaiveBayesLearningAlgorithm()
  // обучить модель
  c.addBaseCSV(pathToBase, patternToParse)
  val cModel = c.classifier


  // классифицировать из JSON
  var jsonStr = JsonFileReader("data/jsonTexts/newsfeedGet_many.json")
  //var jsonStr = JsonFileReader("data/jsonTexts/newsfeedGet_communitySigned.json")
  //var jsonStr = JsonFileReader("data/jsonTexts/newsfeedGet_communityUnsigned.json")

  // из JSON который возвращает все items со стены прочтет первый итем как VkNewsFeedPost,
  // и если это не VkNewsFeedPost, то вернет ошибку валидации
  // не нужен, подумаю как с ним быть
  val full = readNewsFeedPost(jsonStr)

  // из JSON который возвращает все items со стены прочтет только VkNewsFeedPost,
  // возвращает Seq[Any], это плохо: приходится брать элементы как инстансы класса :с
  val vkPosts = readNewsFeedItems[VkNewsFeedPost](jsonStr)

  var seq = vkPosts.map({
    case p if {
      p.sourceId < 0 && p.signerId.isDefined ||
        p.sourceId > 0
    } =>
      readUser(JsonFileReader("data/jsonTexts/usersGet.json")) match {
        case Right(u:VkUser) =>
          NewsFeedItem(
            p.sourceId,
            p.text,
            cModel.classify(filterTwitter(p.text)),
            u.firstName,
            u.lastName,
            u.photo100)
        case Left(_) => NewsFeedItem()
      }
    case p if p.sourceId < 0 && p.signerId.isEmpty =>
      readCommunity(JsonFileReader("data/jsonTexts/groupsGetById.json")) match {
        case Right(c:VkCommunity) =>
          NewsFeedItem(
            p.sourceId,
            p.text,
            cModel.classify(filterTwitter(p.text)),
            c.name,
            c.name,
            c.photo100)
        case Left(_) => NewsFeedItem()
      }
  })

  println(seq)

}
