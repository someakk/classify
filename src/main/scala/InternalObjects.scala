
case class NewsFeedItem(id: Int = 0,
                        text: String = "",
                        text_class: String = "",
                        first_name: String = "",
                        last_name: String = "",
                        photo_100: String = "")
