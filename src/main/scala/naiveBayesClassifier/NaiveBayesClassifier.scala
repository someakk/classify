package naiveBayesClassifier

/**
 * Алгоритм непосредственно классификации
 * @param m статистическая модель классификатора
 */
class NaiveBayesClassifier(m: NaiveBayesModel) {

	def classify(s: String) = {
//		m.classes
//			.toList
//			.map(c => (c, calculateProbability(c, s)))
//			.maxBy(_._2)
//			._1

		val cList = m.classes.toList
		val mMap = cList.map(c => {
			val res = calculateProbability(c, s)
			val t = (c, res)
			t
		})
		//будет критично если имеется много классов
		val mMax = mMap.maxBy(_._2)
		val v1 = mMax._1
		v1
	}

	def tokenize(s: String) = s.split(' ')

	/**
	 * Рассчитывает оценку вероятности документа в пределах класса
	 * @param c класс
	 * @param s текст документа
	 * @return оценка <code>P(c|d)</code>
	 */
	def calculateProbability(c: String, s: String) = {

		//		val toks = tokenize(s)
		//		val mMap = toks.map(word => {
		//			val w = word
		//			m.wordLogProbability(c, word)
		//		})
		//		val Sum = mMap.sum
		//		val res = Sum + m.classLogProbability(c)
		//		res
		tokenize(s)
			.map(m.wordLogProbability(c, _))
			.sum
		+ m.classLogProbability(c)

	}
}
