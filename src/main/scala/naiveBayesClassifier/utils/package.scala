package naiveBayesClassifier

import textFilter.Filter._
import scala.util.matching.Regex

package object utils {

  implicit class StringImprovements(val that: NaiveBayesLearningAlgorithm) {

    def addBaseCSV(path: String, patternToParse: String): NaiveBayesClassifier = {

      val bufferedSource = io.Source.fromFile(path)
      val pattern: Regex = patternToParse.r

      for (line <- bufferedSource.getLines) {

        for (matchedString <- pattern.findAllMatchIn(line)){

          var text = matchedString.
            group(1).
            slice(1, matchedString.group(1).length-1)

          val typeClass = matchedString.
            group(2).
            slice(1, matchedString.group(2).length-1)

          // нормализация текста
          text = filterTwitter(text)

          // добавляем в модель
          that.addExample(text, typeClass)

        }
      }

      bufferedSource.close
      that.classifier
    }

  }

}
