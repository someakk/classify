package jsonReader

object FileReader {

  def JsonFileReader (filePath: String): String = {
    val bufferedSource = io.Source.fromFile(filePath)
    val jsonStr = bufferedSource.mkString
    bufferedSource.close
    jsonStr
  }

}
