package jsonReader
import api.vk.attachments.VkVideo

import scala.reflect.ClassTag
import play.api.libs.json._
import api.vk.{VkCommunity, VkUser}
import api.vk.newsFeed.{FeedItemLike, VkNewsFeedItem}
import api.vk.newsFeed.vkNewsFeedPost.VkNewsFeedPost

object ApiVkReader extends App{

  // todo решить за актуальность, сейчас используется readNewsFeedItems
  def readNewsFeedPost (json: String): Any = {
    val obj: JsValue = Json.parse(json)
    ( obj \ "response" \ "items" \ 0 ).validate[VkNewsFeedPost] match {
      case JsSuccess(value, _) => Right(value)
      case JsError(errors) => Left(new Exception("Unknown object:" + errors.map(_.toString()).toString()))
    }
  }

  def isSuchClassAs[T: ClassTag](that: Any): Boolean = {
    that match {
      case suchClassAsThat:T => true
      case notSuchClassAsThat @ _ => false
    }
  }

  def readNewsFeedItems [T: ClassTag : FeedItemLike](json: String): Seq[T] = {
    val obj: JsValue = Json.parse(json)
    val seq = ( obj \ "response" \ "items").validate[Array[VkNewsFeedItem]]
    seq
      .get
      .filter(item => isSuchClassAs[T](item.feedItem))
      .collect(_.feedItem.asInstanceOf[T])
  }

  def readUser (json: String): Any = {
    val obj: JsValue = Json.parse(json)
    ( obj \ "response" \ 0 ).validate[VkUser] match {
      case JsSuccess(value, _) => Right(value)
      case JsError(errors) => Left(new Exception("Unknown object:" + errors.map(_.toString()).toString()))
    }
  }

  def readCommunity (json: String): Any = {
    val obj: JsValue = Json.parse(json)
    ( obj \ "response" \ 0 ).validate[VkCommunity] match {
      case JsSuccess(value, _) => Right(value)
      case JsError(errors) => Left(new Exception("Unknown object:" + errors.map(_.toString()).toString()))
    }
  }

  def readVideo (json: String): Any = {
    val obj: JsValue = Json.parse(json)
    val vld = ( obj \ "response" ).validate[VkVideo]
    vld match {
      case JsSuccess(value, _) => Right(value)
      case JsError(errors) => Left(new Exception("Unknown object:" + errors.map(_.toString()).toString()))
    }
  }


}
